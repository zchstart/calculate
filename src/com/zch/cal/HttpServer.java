package com.zch.cal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @Description
 * @Author zhangchenhui
 * @Date 2021/8/3 20:35
 */
public class HttpServer implements Runnable {
    private ServerSocket serverSocket;

    public HttpServer(int port) {
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void start() {
        new Thread(this).start();
    }

    @Override
    public void run() {
        while (true) {
            Socket socket = null;
            try {
                socket = serverSocket.accept();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String line = null;
                if ((line = bufferedReader.readLine()) != null && !line.equals("")) {
                    stringBuilder.append(line);
                }
                String url = stringBuilder.substring(4, stringBuilder.lastIndexOf("HTTP/1.1") - 1);
                System.out.println(url);
                PrintWriter printWriter = new PrintWriter(
                        socket.getOutputStream(), true);
                doService(printWriter, url);
                printWriter.close();
                bufferedReader.close();
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void doService(PrintWriter printWriter, String url) {
        String result = handleUrl(url);
        String str = "HTTP/1.1 200 OK " +
                "Server:myServer" +
                "Content-Type: text/html" +
                "\n" +
                "\n" +
                "<h1>" + result + "</h1>";
        printWriter.write(str);
    }

    /**
     * 对查询字符串进行处理，求出结果
     *
     * @param url
     * @return
     */
    private String handleUrl(String url) {
        String result = "";
        url = url.substring(1);
        if (url.length() == 0) {
            return "Empty url !";
        }
        if (url.indexOf("?") < 0) {
            return "No parameters !";
        }
        String[] split = url.split("\\?");
        int num = 0;
        switch (split[0]) {
            case "add":
                return doHandle(url);
            case "mult":
                return doHandle(url);
            default:
                return "i don't know !";
        }
    }

    private String doHandle(String url) {
        String query = url.substring(4);
        if (query.indexOf('&') < 0) {
            return "Less than two parameters";
        }
        int result = 0;
        String[] params = query.split("&");
        for (String value : params) {
            if (value.indexOf('=') < 0) {
                return "请输入一个以上参数";
            }
            if (url.startsWith("add")) {
                result += Integer.parseInt(value.substring(value.indexOf('=') + 1));
            } else if (url.startsWith("mult")) {
                result = 1;
                result *= Integer.parseInt(value.substring(value.indexOf('=') + 1));
            }
        }
        return String.valueOf(result);
    }
}
